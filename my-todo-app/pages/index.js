import { useState } from 'react';

export default function Home() {
  const [todos, setTodos] = useState([]);
  const [inputValue, setInputValue] = useState('');

  function handleInputChange(event) {
    setInputValue(event.target.value);
  }

  function handleAddTodo() {
    if (inputValue.trim()) {
      setTodos([...todos, { text: inputValue.trim(), completed: false }]);
      setInputValue('');
    }
  }

  function handleToggleTodoCompleted(index) {
    setTodos(todos.map((todo, i) => {
      if (i === index) {
        return { ...todo, completed: !todo.completed };
      }
      return todo;
    }));
  }

  function handleDeleteTodo(index) {
    setTodos(todos.filter((_, i) => i !== index));
  }

  return (
    <div className="bg-gray-100  flex flex-col justify-center items-center w-[500px] min-h-screen">
      <div className="bg-white rounded-lg shadow-lg p-6">
        <h1 className="text-3xl font-bold mb-6">Todo List</h1>
        <div className="flex mb-6">
          <input
            className="rounded-l-lg border-gray-400 py-2 px-4 mr-0 leading-tight focus:outline-none"
            type="text"
            value={inputValue}
            onChange={handleInputChange}
          />
          <button
            className="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-r-lg"
            onClick={handleAddTodo}
          >
            Add Todo
          </button>
        </div>
        <ul className="list-disc list-inside">
          {todos.map((todo, index) => (
            <li
              key={index}
              className={`flex items-center justify-between py-2 ${index % 2 === 0 ? 'bg-gray-100' : 'bg-gray-200'}`}
            >
              <div className="flex items-center">
                <input
                  className="mr-2 leading-tight"
                  type="checkbox"
                  checked={todo.completed}
                  onChange={() => handleToggleTodoCompleted(index)}
                />
                <span className={todo.completed ? 'line-through text-gray-400' : 'text-gray-800'}>
                  {todo.text}
                </span>
              </div>
              <button
                className="bg-red-500 hover:bg-red-700 text-white font-bold py-1 px-2 rounded-lg"
                onClick={() => handleDeleteTodo(index)}
              >
                Delete
              </button>
            </li>
          ))}
        </ul>
      </div>
    </div>
  );
}
